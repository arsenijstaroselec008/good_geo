# Convertation Tables app

## Создание exe файла

- Скачать необходимые библиотеки

```bash
pip install -r requirements.txt
pip install auto-py-to-exe
```

- Запустить auto-py-to-exe в консоли питона

```bash
python -m auto_py_to_exe
```

или запустить auto_py_to_exe.exe из папки в которой он расположен

- Настроить AutoPyToExe

**Script Location:** main.py

**Onefile:** One file

**Console Window:** Window based (hide the console)

**Icon:** logo.ico

**Additional files:** customtkinter/, formulas/

**Settings:** Output Directory - выбрать папку где будет создан exe

- Запустить генерацию файла

Внизу окна нажать "CONVERT .PY TO .EXE"