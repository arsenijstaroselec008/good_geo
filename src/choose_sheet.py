from openpyxl import load_workbook
from ui.widgets.label import LabelWidget
from ui.widgets.optionmenu import OptionMenuWidget
from ui.widgets.button import ButtonWidget
from ui.widgets.window import MainWindow
from table_utilits import PAD_X, PAD_Y

def organization_selection_sheet_and_check_hidden_sheets(user_input_direct, selected_sheet, window: MainWindow):
    workbook = load_workbook(user_input_direct)
    sheets = workbook.sheetnames
    visible_sheets = []

    for sheet_name in sheets:
        sheet = workbook[sheet_name]
        if sheet.sheet_state == 'visible':
            visible_sheets.append(sheet.title)

    selected_sheet.set(visible_sheets[0])

    LabelWidget(window.window, "Пожалуйста, выберите страницу Excel из списка ниже:").pack()
    OptionMenuWidget(window.window, selected_sheet, visible_sheets).option_menu.pack(padx=PAD_X, pady=PAD_Y)
    ButtonWidget(window.window, "Выбрать", lambda:(window.clear_window(), window.window.quit())).pack(padx=PAD_X, pady=PAD_Y)

    window.run()
    workbook.close()
