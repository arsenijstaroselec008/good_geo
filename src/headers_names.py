from table_utilits import PopularTermins, UniversalTermins

def table_version_1() -> dict:
    required_values = {
        "A3":PopularTermins.PLAST.value,
        "B3":PopularTermins.DEPOSIT.value,
        "C3":PopularTermins.CATEGORY.value,
        "D3":PopularTermins.ZONE.value,
        "E3":PopularTermins.OIL_BEARING_AREA.value,
        "F3":PopularTermins.AVERAGE_OIL.value,
        "G3":PopularTermins.VOLUME_OIL.value,
        "H3":PopularTermins.COEFFICENTES.value,
        "K3":PopularTermins.OIL_DEBCITY.value,
        "L3":PopularTermins.INITAL_GEO_RESERVES.value,
        "M3":PopularTermins.COEFFICENTES_OIL.value,
        "M5":PopularTermins.TECHNOLOGAL.value,
        "N5":PopularTermins.RENTABLE.value,
        "O3":PopularTermins.INITAL_RETRIEVABLE_RESERVES.value,
        "P3":PopularTermins.INITAL_RETRIEVABLE_REINTABLE_RESERVES.value,
        "Q3":PopularTermins.GAS.value
    }
    return required_values

def table_gas_cup_gas() -> dict:
    required_values = {
        "A3":PopularTermins.PLAST.value,
        "B3":PopularTermins.DEPOSIT.value,
        "C3":PopularTermins.CATEGORY.value,
        "D3":PopularTermins.ZONE.value,
        "E3":PopularTermins.OIL_BEARING_AREA.value,
        "F3":PopularTermins.AVERAGE_OIL.value,
        "G3":PopularTermins.VOLUME_GAS.value,
        "H3":PopularTermins.COEFFICENTES.value,
        "H4":PopularTermins.OPEN_PORISTOSY.value,
        "I4":PopularTermins.GAS_SATURATION.value,
        "J3":PopularTermins.RESERVOIR_PRESSURE.value,
        "L3":PopularTermins.AMEDMENT.value,
        "N3":PopularTermins.INITAL_GEO_RESERVES.value,
        "O3":PopularTermins.SHARE_SUHOGO_GAS.value,
        "Q3":PopularTermins.KIG.value,
        "S3":PopularTermins.INITAL_RETRIEVABLE_RESERVES.value,
        "V3":PopularTermins.CONDENCAT.value,
        "W3":PopularTermins.KIK.value,
        "Y3":PopularTermins.CONDENCAT_RESERVES.value,
        "AA3":PopularTermins.PORE_VOLUME.value,
        "AB3":PopularTermins.GAS_SATURATION_VOLUME.value
        }
    return required_values

def table_version_1_extra_columns() -> dict:
    required_values = {
        "A3":PopularTermins.PLAST.value,
        "B3":PopularTermins.DEPOSIT.value,
        "C3":PopularTermins.CATEGORY.value,
        "D3":PopularTermins.ZONE.value,
        "E3":PopularTermins.OIL_BEARING_AREA.value,
        "F3":PopularTermins.AVERAGE_OIL.value,
        "G3":PopularTermins.VOLUME_OIL.value,
        "H3":PopularTermins.COEFFICENTES.value,
        "K3":PopularTermins.OIL_DEBCITY.value,
        "L3":PopularTermins.INITAL_GEO_RESERVES.value,
        "M3":PopularTermins.COEFFICENTES_OIL.value,
        "O3":PopularTermins.INITAL_RETRIEVABLE_RESERVES.value,
        "P3":PopularTermins.INITAL_RETRIEVABLE_REINTABLE_RESERVES.value,
        "Q3":PopularTermins.GAS.value,
        "T3":PopularTermins.PORE_VOLUME.value,
        "U3":PopularTermins.OIL_SATURATION_VOLUME.value,
        "V3":PopularTermins.OIL_BEARING_AREA.value,
        "W3":UniversalTermins.AREA_DIFFERENCE.value,
        "X3":PopularTermins.AVERAGE_OIL.value,
        "Y3":UniversalTermins.DIFFERENCE_AVERAGE.value,
        "AB3":UniversalTermins.POR_COEF_VAR2.value,
        "AC3":UniversalTermins.POR_VOL_OIL_VOL.value,
        "AD3":UniversalTermins.OIL_COEF_VAR2.value,
        "AE3":UniversalTermins.OIL_VOL_VAR2.value,
        "AF3":UniversalTermins.VOL_OIL_ROCKS_VAR3.value,
        "AG3":UniversalTermins.VOL_OIL_ROCKS_VAR3_DIF.value,
        "AH3":UniversalTermins.OIL_DENS_VAR2.value,
        "AI3":UniversalTermins.OIL_DENS_DIF.value,
        "AJ3":UniversalTermins.CONV_FACT_VAR2.value,
        "AK3":UniversalTermins.CONV_FACT_DIF.value,
        "AL3":UniversalTermins.INIT_REC_TECH_OIL_VAR2.value,
        "AM3":UniversalTermins.KIN_TECH_RES_VAR2.value,
        "AN3":UniversalTermins.INIT_REC_RENT_OIL_VAR2.value,
        "AO3":UniversalTermins.KIN_RENT_RES_VAR2.value,
        "AP3":UniversalTermins.GAS_CON_VAR2.value,
        "AQ3":UniversalTermins.GEO_RES_PART_VAR2.value,
        "AR3":UniversalTermins.INIT_TECH_RES_PART.value,
        "AS3":UniversalTermins.INIT_REC_RES_PART.value,
        "AT3":UniversalTermins.GAS_FACTOR.value
    }
    return required_values

def pz_verson_one() -> dict:
    required_values = {
        "A3":PopularTermins.PLAST.value,
        "B3":PopularTermins.DEPOSIT.value,
        "C3":PopularTermins.CATEGORY.value,
        "D3":PopularTermins.ZONE.value,
        "E3":PopularTermins.OIL_BEARING_AREA.value,
        "G3":PopularTermins.VOLUME_OIL.value,
        "L3":PopularTermins.INITAL_GEO_RESERVES.value,
        "F3":PopularTermins.EFFECTIVE_THICKNESS.value,
        "H3":PopularTermins.COEFFICENTES.value,
        "K3":PopularTermins.OIL_DEBCITY.value,
        "M3":PopularTermins.COEFFICENTES_OIL.value,
        "O3":PopularTermins.INITAL_RETRIEVABLE_RESERVES.value,
        "Q3":PopularTermins.ACCUMULATED_PRODUCTION.value,
        "R3":PopularTermins.RESIDUAL_RESERVES.value,
        "U3":PopularTermins.GAS.value,
        "V3":PopularTermins.INITAL_GEO_RESERVES.value,
        "W3":PopularTermins.ININTAL_TECHONOLOGICAL.value,
        "X3":PopularTermins.ACCUMULATED_PRODUCTION.value,
        "Y3":PopularTermins.RESIDUAL_RESERVES.value,
        "AA3":PopularTermins.PORE_VOLUME.value,
        "AB3":PopularTermins.OIL_SATURATION_VOLUME.value
    }
    return required_values

def pz_version_two() -> dict:
    required_values = {
        "A3":PopularTermins.PRODUCTIVE_STRATUM.value,
        "B3":PopularTermins.DEPOSIT.value,
        "C3":PopularTermins.CATEGORY.value,
        "D3":PopularTermins.ZONE.value
    }
    return required_values

def pz_version_three() -> dict:
    required_values = {
        "A4":PopularTermins.PLAST.value,
        "B4":PopularTermins.DEPOSIT.value,
        "C4":PopularTermins.CATEGORY.value,
        "D4":UniversalTermins.ININTAL_RESERVES.value,
        "E6":UniversalTermins.TECHNOLOGIVAL_RESERVES.value,
        "F4":UniversalTermins.POTENTIAL_CONTENT.value,
        "F5":UniversalTermins.HELIUM.value,
        "G5":UniversalTermins.ETHANE.value,
        "I5":UniversalTermins.PROPANE.value,
        "K5":UniversalTermins.BUTANE.value,
        "M4":PopularTermins.INITAL_GEO_RESERVES.value,
        "M5":UniversalTermins.HELIUM.value,
        "N5":UniversalTermins.ETHANE.value,
        "O5":UniversalTermins.PROPANE.value,
        "P5":UniversalTermins.BUTANE.value,
        "Q4":PopularTermins.ININTAL_TECHONOLOGICAL.value,
        "Q5":UniversalTermins.HELIUM.value,
        "R5":UniversalTermins.ETHANE.value,
        "S5":UniversalTermins.PROPANE.value,
        "T5":UniversalTermins.BUTANE.value,
        "U4":PopularTermins.ACCUMULATED_PRODUCTION.value,
        "V5":UniversalTermins.HELIUM.value,
        "W5":UniversalTermins.ETHANE.value,
        "X5":UniversalTermins.PROPANE.value,
        "Y5":UniversalTermins.BUTANE.value,
        "Z4":PopularTermins.RESIDUAL_RESERVES.value,
        "Z5":UniversalTermins.HELIUM.value,
        "AA5":UniversalTermins.ETHANE.value,
        "AB5":UniversalTermins.PROPANE.value,
        "AC5":UniversalTermins.BUTANE.value,
        "AD4":PopularTermins.RESIDUAL_RESERVES.value,
        "AD5":UniversalTermins.HELIUM.value,
        "AE5":UniversalTermins.ETHANE.value,
        "AF5":UniversalTermins.PROPANE.value,
        "AG5":UniversalTermins.BUTANE.value,
    }
    return required_values

def pz_version_four() -> dict:
    required_values = {
        "A4":PopularTermins.PLAST.value,
        "B4":PopularTermins.DEPOSIT.value,
        "C4":PopularTermins.CATEGORY.value,
        "D4":UniversalTermins.ININTAL_RESERVES.value,
        "F4":UniversalTermins.POTENTIAL_CONTENT.value,
        "L4":PopularTermins.INITAL_GEO_RESERVES.value,
        "O4":PopularTermins.ININTAL_TECHONOLOGICAL.value,
        "R4":PopularTermins.ACCUMULATED_PRODUCTION.value,
        "V4":PopularTermins.VOLUME_UPLOADED_GAS.value,
        "AC4":PopularTermins.VOLUME_GAS_COMPONENT.value,
        "AF4":PopularTermins.RESIDUAL_RESERVES.value

    }
    return required_values

def pz_version_six():
    required_values = {
        "A4":PopularTermins.PRODUCTIVE_STRATUM.value,
        "B4":PopularTermins.DEPOSIT.value,
        "C4":PopularTermins.CATEGORY.value,
        "D4":UniversalTermins.ININTAL_OIL_RESERVES.value,
        "D6":UniversalTermins.GEOLOGIC.value,
        "E6":UniversalTermins.TECHNOLOGIVAL_RESERVES.value,
        "F4":UniversalTermins.SULFUR_CONTENT.value,
        "G4":UniversalTermins.INITIAL_SULFUR_RESERVES.value,
        "G6":UniversalTermins.GEOLOGIC.value,
        "H6":UniversalTermins.TECHNOLOGIVAL_RESERVES.value,
        "I4":PopularTermins.ACCUMULATED_PRODUCTION.value,
        "I6":UniversalTermins.OILS.value,
        "J6":UniversalTermins.SULFURS.value,
        "K4":UniversalTermins.RESIDUAL_SULFUR_RESERVES.value,
        "K6":UniversalTermins.GEOLOGIC.value,
        "L6":UniversalTermins.TECHNOLOGIVAL_RESERVES.value,
    }
    return required_values

def pz_version_five() -> dict:
    required_values = {
        "A4":PopularTermins.PLAST.value,
        "B4":PopularTermins.DEPOSIT.value,
        "C4":PopularTermins.CATEGORY.value,
        "D4":UniversalTermins.ININTAL_RESERVES.value,
        "F4":UniversalTermins.POTENTIAL_CONTENT.value,
        "L4":PopularTermins.INITAL_GEO_RESERVES.value,
        "O4":PopularTermins.ININTAL_TECHONOLOGICAL.value,
        "R4":PopularTermins.ACCUMULATED_PRODUCTION.value,
        "V4":PopularTermins.VOLUME_UPLOADED_GAS.value,
        'W4':UniversalTermins.POTENTIAL_CONTENT.value,
        'AC4':PopularTermins.VOLUME_GAS_COMPONENT.value,
        'AF4':PopularTermins.RESIDUAL_RESERVES.value,
        'AI4':PopularTermins.RESIDUAL_RESERVES.value
    }
    return required_values
