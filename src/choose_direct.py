from customtkinter import filedialog

def choose_directories():

    file_path = filedialog.askopenfilename(filetypes=[("Excel files", "*.xlsx")])

    if file_path:
        return file_path
