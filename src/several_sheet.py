from openpyxl import load_workbook
from ui.widgets.label import LabelWidget
from ui.widgets.optionmenu import OptionMenuWidget
from ui.widgets.button import ButtonWidget
from ui.widgets.window import MainWindow
from table_utilits import PAD_Y

def selecting_multiple_sheets(user_input_direct, selected_sheet, selected_sheet_two, window: MainWindow):
    workbook = load_workbook(user_input_direct)
    sheets = workbook.sheetnames
    visible_sheets = []

    for sheet_name in sheets:
        sheet = workbook[sheet_name]
        if sheet.sheet_state == 'visible':
            visible_sheets.append(sheet.title)

    selected_sheet.set(visible_sheets[0])
    selected_sheet_two.set(visible_sheets[1])


    label_widget = LabelWidget(window.window, "Пожалуйста, выберите основной расчетный лист:", wraplength=260)
    label_widget.widget.grid(row=1, column=0, padx=(50, 10), pady=PAD_Y, sticky="e")
    OptionMenuWidget(window.window, selected_sheet, visible_sheets).option_menu.grid(row=1, column=1, padx=(10, 50), pady=PAD_Y, sticky="nsew", )
    LabelWidget(window.window, "Пожалуйста, выберите лист с дополнительными данными:", wraplength=260).widget.grid(row=2, column=0, padx=(50, 10), pady=PAD_Y, sticky="nsew")
    OptionMenuWidget(window.window, selected_sheet_two, visible_sheets).option_menu.grid(row=2, column=1, padx=(10, 50), pady=PAD_Y, sticky="nsew")
    ButtonWidget(window.window, "Выбрать", lambda:(window.clear_window(), window.window.quit())).widget.grid(row=3, column=1, padx=(10, 50), pady=PAD_Y, sticky="nsew") #, bg="black", fg="white"

    window.run()
    workbook.close()