from openpyxl import load_workbook, worksheet
from ui.widgets.label import LabelWidget
from ui.widgets.textbox import TextBoxWidget
from ui.widgets.button import ButtonWidget
from ui.widgets.window import MainWindow
from table_utilits import PAD_X, PAD_Y
from headers_names import table_version_1, table_gas_cup_gas, table_version_1_extra_columns, pz_verson_one, pz_version_two, pz_version_three, pz_version_four, pz_version_five, pz_version_six

def open_sheet_excel(user_direct, sheet_name) -> worksheet:
    workbook = load_workbook(filename=user_direct, read_only=True)
    sheet = workbook[sheet_name]
    return sheet

def check_headers(sheet:worksheet, required_values:dict, window:MainWindow, sheetname:str) -> bool:
    error_messages = []
    validation_result = True

    for cell, value in required_values.items():
        if sheet[cell].value is not None:
            current_cell = str(sheet[cell].value)
            if str(value.lower()) not in current_cell.lower():
                error_message = f'Ошибка: ключевое слово "{value}" не найдено в ячейке {cell}'
                error_messages.append(error_message)

        elif sheet[cell].value is None:
            error_message = f'Ошибка: ключевое слово "{value}" не найдено в ячейке {cell}'
            error_messages.append(error_message)

    if error_messages:
        LabelWidget(window.window, f'Обнаружены несоответствия в наименованиях в титуле шапки в "{sheetname}"\n Пожалуйста, исправьте указанные несоответствия и перезапустите процесс').pack(pady=PAD_Y)

        error_text = TextBoxWidget(window.window, 700, 300)
        error_text.pack()

        for error_message in error_messages:
            error_text.insert_text(error_message + "\n")
        validation_result = False

    else:
        LabelWidget(window.window, "Проверка шапки пройдена успешно, мы готовы приступить к вычислениям").pack()
        ButtonWidget(window.window, "Продолжить", lambda:(window.clear_window(), window.window.quit())).pack(padx=PAD_X, pady=PAD_Y)
    window.run()

    return validation_result

def is_header_valid(user_direct, sheet_name, window: MainWindow) -> bool:
    sheet = open_sheet_excel(user_direct, sheet_name)
    required_values = table_version_1()

    return check_headers(sheet, required_values, window, sheet_name)

def check_header_table_opz_221(user_direct, sheet_name, window: MainWindow) -> bool:
    sheet = open_sheet_excel(user_direct, sheet_name)
    required_values = table_gas_cup_gas()

    return check_headers(sheet, required_values, window, sheet_name)

def validate_header_tab_1_0_extra_cols(user_direct, sheet_name, window: MainWindow) -> bool:
    sheet = open_sheet_excel(user_direct, sheet_name)
    required_values = table_version_1_extra_columns()

    return check_headers(sheet, required_values, window, sheet_name)

def validate_header_pz(user_direct, sheet_name, window: MainWindow) -> bool:
    sheet = open_sheet_excel(user_direct, sheet_name)
    required_values = pz_verson_one()

    return check_headers(sheet, required_values, window, sheet_name)

def validate_header_pz_gaz(user_direct, sheet_name, window: MainWindow) -> bool:
    sheet = open_sheet_excel(user_direct, sheet_name)
    required_values = pz_version_two()

    return check_headers(sheet, required_values, window, sheet_name)

def validate_header_pz_1_3(user_direct, sheet_name, window: MainWindow) -> bool:
    sheet = open_sheet_excel(user_direct, sheet_name)
    required_values = pz_version_three()

    return check_headers(sheet, required_values, window, sheet_name)

def validate_header_pz_1_4(user_direct, sheet_name, window: MainWindow) -> bool:
    sheet = open_sheet_excel(user_direct, sheet_name)
    required_values = pz_version_four()

    return check_headers(sheet, required_values, window, sheet_name)

def validate_header_pz_1_6(user_direct, sheet_name, window: MainWindow) -> bool:
    sheet = open_sheet_excel(user_direct, sheet_name)
    required_values = pz_version_six()

    return check_headers(sheet, required_values, window, sheet_name)

def validate_header_pz_1_5(user_direct:str, sheet_name:str, window:MainWindow) -> bool:
    sheet = open_sheet_excel(user_direct, sheet_name)
    required_values = pz_version_five()

    return check_headers(sheet, required_values, window, sheet_name)
