import customtkinter as ctk
from table_utilits import (
    convert_tab_opz_1_0,
    convert_tab_opz_1_1,
    convert_tab_opz_1_0_extra_cols,
    convert_tab_pz_1_1,
    convert_tab_pz_1_2,
    convert_tab_pz_1_3,
    convert_tab_pz_1_4,
    TableController,
    convert_tab_pz_1_6
)
from table_utilits import TableType, SelectOperationRadioValues, GroupsRadioValues, Material, TempalteRadioValues
from choose_direct import choose_directories
from excel_conversion_controller import ExcelConversionController
from several_sheet import selecting_multiple_sheets
from choose_sheet import organization_selection_sheet_and_check_hidden_sheets
from menu import generate_radio_buttons
from excel_converter import ExcelConverter
from excel_comparator import ExcelComparator
from ui.widgets.window import MainWindow
from select_operation import select_operation
from check_shapka import (
    is_header_valid,
    check_header_table_opz_221,
    validate_header_tab_1_0_extra_cols,
    validate_header_pz,
    validate_header_pz_gaz,
    validate_header_pz_1_3,
    validate_header_pz_1_4,
    validate_header_pz_1_5,
    validate_header_pz_1_6
)

ctk.set_appearance_mode("Dark")

window = MainWindow()

def choose_sheet(user_input_direct):
    if user_input_direct is not None:
        selected_sheet = ctk.StringVar(window.window)
        organization_selection_sheet_and_check_hidden_sheets(user_input_direct, selected_sheet, window)
        user_input_sheet = selected_sheet.get()

        return user_input_sheet

def choose_several_sheet(user_input_direct):
    if user_input_direct is not None:
        selected_sheet = ctk.StringVar(window.window)
        selected_sheet_two = ctk.StringVar(window.window)
        selecting_multiple_sheets(user_input_direct, selected_sheet, selected_sheet_two, window)
        user_input_sheet = [selected_sheet.get(), selected_sheet_two.get()]

        return user_input_sheet

def shablon_opz_one(user_input_direct):
    user_input_sheet = choose_sheet(user_input_direct)
    if is_header_valid(user_input_direct, user_input_sheet, window):
        convertation_table = ExcelConverter(user_input_direct, user_input_sheet, convert_tab_opz_1_0, window)
        convertation_table.start_excel_conversion()

def shablon_opz_one_gas(user_input_direct):
    user_input_sheet = choose_sheet(user_input_direct)
    if check_header_table_opz_221(user_input_direct, user_input_sheet, window):
        convertation_table = ExcelConverter(user_input_direct, user_input_sheet, convert_tab_opz_1_1, window)
        convertation_table.start_excel_conversion()

def shablon_opz_one_extra_columns(user_input_direct):
    user_input_sheet = choose_sheet(user_input_direct)
    if validate_header_tab_1_0_extra_cols(user_input_direct, user_input_sheet, window):
        convertation_table = ExcelConverter(user_input_direct, user_input_sheet, convert_tab_opz_1_0_extra_cols, window)
        convertation_table.start_excel_conversion()

def shablon_pz_one(user_input_direct, state):
    user_input_sheet = choose_sheet(user_input_direct)
    if validate_header_pz(user_input_direct, user_input_sheet, window):
        if SelectOperationRadioValues.CONVERT in state:
            convertation_table = ExcelConverter(user_input_direct, user_input_sheet, convert_tab_pz_1_1, window)
            convertation_table.start_excel_conversion()
        elif SelectOperationRadioValues.COMPARE in state:
            compare_table = ExcelComparator(user_input_direct, user_input_sheet, TableType.PZ_1_1, window)
            compare_table.start_comparison()

def shablon_pz_two(user_input_direct, state):
    user_input_sheet = choose_sheet(user_input_direct)
    if validate_header_pz_gaz(user_input_direct, user_input_sheet, window):
        if SelectOperationRadioValues.CONVERT in state:
            convertation_table = ExcelConverter(user_input_direct, user_input_sheet, convert_tab_pz_1_2, window)
            convertation_table.start_excel_conversion()
        elif SelectOperationRadioValues.COMPARE in state:
            compare_table = ExcelComparator(user_input_direct, user_input_sheet, TableType.PZ_1_2, window)
            compare_table.start_comparison()

def shablon_pz_three(user_input_direct, state):
    user_input_sheet = choose_several_sheet(user_input_direct)
    if validate_header_pz_1_3(user_input_direct, user_input_sheet[0], window):
        if SelectOperationRadioValues.CONVERT in state:
            convertation_table = ExcelConverter(user_input_direct, user_input_sheet, convert_tab_pz_1_3, window)
            convertation_table.start_excel_conversion()
        elif SelectOperationRadioValues.COMPARE in state:
            compare_table = ExcelComparator(user_input_direct, user_input_sheet, TableType.PZ_1_3, window)
            compare_table.start_comparison()

def shablon_pz_four(user_input_direct, state):
    user_input_sheet = choose_several_sheet(user_input_direct)
    if validate_header_pz_1_4(user_input_direct, user_input_sheet[0], window):
        if SelectOperationRadioValues.CONVERT in state:
            convertation_table = ExcelConverter(user_input_direct, user_input_sheet, convert_tab_pz_1_4, window)
            convertation_table.start_excel_conversion()
        elif SelectOperationRadioValues.COMPARE in state:
            compare_table = ExcelComparator(user_input_direct, user_input_sheet, TableType.PZ_1_4, window)
            compare_table.start_comparison()

def shablon_pz_five(user_input_direct, state):
    user_input_sheet = choose_several_sheet(user_input_direct)
    if validate_header_pz_1_5(user_input_direct, user_input_sheet[0], window):
        if SelectOperationRadioValues.CONVERT in state:
            conversion_controller = TableController(user_input_direct, user_input_sheet)
            convertation_table = ExcelConversionController(user_input_direct, user_input_sheet, conversion_controller.start_converter, window)
            convertation_table.start_excel_conversion()
        elif SelectOperationRadioValues.COMPARE in state:
            compare_table = ExcelComparator(user_input_direct, user_input_sheet, TableType.PZ_1_5, window)
            compare_table.start_comparison()

def shablon_pz_six(user_input_direct, state):
    user_input_sheet = choose_several_sheet(user_input_direct)
    if validate_header_pz_1_6(user_input_direct, user_input_sheet[0], window):
        if SelectOperationRadioValues.CONVERT in state:
            convertation_table = ExcelConverter(user_input_direct, user_input_sheet, convert_tab_pz_1_6, window)
            convertation_table.start_excel_conversion()
        elif SelectOperationRadioValues.COMPARE in state:
            compare_table = ExcelComparator(user_input_direct, user_input_sheet, TableType.PZ_1_6, window)
            compare_table.start_comparison()

def choose_strategy(state:list):
    window.clear_window()

    def process_user_selections():
        user_input_direct = choose_directories()

        return user_input_direct

    user_input_direct = process_user_selections()

    if all(element in [SelectOperationRadioValues.CONVERT, TempalteRadioValues.OPZ, GroupsRadioValues.SUMMARY_TABLE_GROUP, Material.OIL_AND_DISSOLVED_GAS] for element in state):
        shablon_opz_one(user_input_direct)
    elif all(element in [SelectOperationRadioValues.CONVERT, TempalteRadioValues.OPZ, GroupsRadioValues.SUMMARY_TABLE_GROUP, Material.FREE_GAS_GGH_CONDENSATE] for element in state):
        shablon_opz_one_gas(user_input_direct)
    elif all(element in [SelectOperationRadioValues.CONVERT, TempalteRadioValues.OPZ, GroupsRadioValues.SUMMARY_TABLE_GROUP, Material.OIL_AND_DISSOLVED_GAS_EXTRA_COLS] for element in state):
        shablon_opz_one_extra_columns(user_input_direct)
    elif TempalteRadioValues.PZ in state and GroupsRadioValues.SUMMARY_TABLE_GROUP in state and Material.OIL_AND_DISSOLVED_GAS in state:
        shablon_pz_one(user_input_direct, state)
    elif TempalteRadioValues.PZ in state and GroupsRadioValues.SUMMARY_TABLE_GROUP in state and Material.FREE_GAS_GGH_CONDENSATE in state:
        shablon_pz_two(user_input_direct, state)
    elif TempalteRadioValues.PZ in state and GroupsRadioValues.SUMMARY_TABLE_GROUP in state and Material.DISSOLVED_GAS_COMPONENTS in state:
        shablon_pz_three(user_input_direct, state)
    elif TempalteRadioValues.PZ in state and GroupsRadioValues.SUMMARY_TABLE_GROUP in state and Material.TABLE_1_4 in state:
        shablon_pz_four(user_input_direct, state)
    elif TempalteRadioValues.PZ in state and GroupsRadioValues.SUMMARY_TABLE_GROUP in state and Material.TABLE_1_5 in state:
        shablon_pz_five(user_input_direct, state)
    elif TempalteRadioValues.PZ in state and GroupsRadioValues.SUMMARY_TABLE_GROUP in state and Material.SUMMARY_TABLE_OF_OIL_AND_SULFUR in state:
        shablon_pz_six(user_input_direct, state)


select_operation(window, command = lambda state:generate_radio_buttons(window, command = choose_strategy, select_operation_state = state))

window.run()
