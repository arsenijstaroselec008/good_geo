import customtkinter as ctk

class OptionMenuWidget:
    def __init__(self, master, variable, values):
        self.option_menu = ctk.CTkOptionMenu(master=master, variable=variable, values=values)
