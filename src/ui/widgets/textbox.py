import customtkinter as ctk
from ui.widgets.base import BaseWidget

class TextBoxWidget(BaseWidget):
    def __init__(self, window, width, height):
        super().__init__(window, ctk.CTkTextbox, width=width, height=height)

    def insert_text(self, text):
        self.widget.insert(ctk.END, text)
