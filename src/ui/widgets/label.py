import customtkinter as ctk
from ui.widgets.base import BaseWidget

class LabelWidget(BaseWidget):
    def __init__(self, window, text, **kwargs):
        super().__init__(window, ctk.CTkLabel, text=text, **kwargs)