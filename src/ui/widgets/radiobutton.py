import customtkinter as ctk
from ui.widgets.base import BaseWidget

class RadioButtonWidget(BaseWidget):
    def __init__(self, window, text, variable, value, command, radiobutton_width = 18, radiobutton_height = 18, border_width_checked =4, border_width_unchecked = 2):
        super().__init__(window,
                         ctk.CTkRadioButton,
                         text = text,
                         variable = variable,
                         value = value,
                         command = command,
                         radiobutton_width = radiobutton_width,
                         radiobutton_height = radiobutton_height,
                         border_width_checked = border_width_checked,
                         border_width_unchecked = border_width_unchecked
                         )
