import customtkinter as ctk
from ui.widgets.base import BaseWidget

class ButtonWidget(BaseWidget):
    def __init__(self, window, text, command, **kwargs):
        super().__init__(window, ctk.CTkButton, text=text, command=command, **kwargs)