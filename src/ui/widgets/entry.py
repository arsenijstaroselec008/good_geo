import customtkinter as ctk
from ui.widgets.base import BaseWidget

class EntryWidget(BaseWidget):
    def __init__(self, window):
        super().__init__(window, ctk.CTkEntry)