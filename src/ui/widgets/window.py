# import sys
# import os
import customtkinter as ctk

# def resource_path(relative_path):
#     base_path = getattr(
#         sys,
#         '_MEIPASS',
#         os.path.dirname(os.path.abspath(__file__)))
#     return os.path.join(base_path, relative_path)
class MainWindow():
    def __init__(self):
        self.window = ctk.CTk()
        self.window.geometry("800x400")
        self.window.title("GoodGEO convertation")
        # icon_relative = "logo.ico"
        # self.icon = resource_path(icon_relative)
        # self.window.iconbitmap(icon_relative)

    def clear_window(self):
        for widget in self.window.winfo_children():
            widget.destroy()

    def run(self):
        self.window.mainloop()
        