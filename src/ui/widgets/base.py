class BaseWidget:
    def __init__(self, window, widget_type, **kwargs):
        self.widget = widget_type(window, **kwargs)

    def pack(self, **kwargs):
        self.widget.pack(**kwargs)

    def update_attribute(self, attribute, value):
        setattr(self.widget, attribute, value)
        self.widget.configure(**{attribute: value})
        