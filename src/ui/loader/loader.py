import customtkinter as ctk
from ui.widgets.window import MainWindow

class LoaderApp:
    def __init__(self, window: MainWindow, on_conversion_complete):
        self.window = window

        self.label = ctk.CTkLabel(self.window.window, text="Ожидание команды...", padx=10, pady=5)
        self.label.pack()

        self.progressbar = ctk.CTkProgressBar(self.window.window, orientation="horizontal", mode="indeterminate")
        self.progressbar.pack(pady=5)

        self.is_loading = False
        self.loader_thread = None
        self.on_conversion_complete = on_conversion_complete
        self.conversion_result = None

    def start_loader(self):
        self.label.configure(text="Идет выполнение вычислений...")
        self.progressbar.start()

    def stop_loader(self):
        self.label.configure(text="Вычисления завершены")
        self.progressbar.stop()
        