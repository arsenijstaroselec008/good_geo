import customtkinter as ctk
from tkinter import ttk
from table_utilits import PAD_X, PAD_Y
from table_utilits import UI_ItemType
from table_utilits import SelectOperationRadioValues
from ui.widgets.radiobutton import RadioButtonWidget
from ui.widgets.button import ButtonWidget
from ui.widgets.label import LabelWidget

def select_operation(window, command):
    operation_group = ctk.StringVar(value=SelectOperationRadioValues.CONVERT.name)
    state = []

    options = [
        {
            'text': 'Выберите операцию',
            'type': UI_ItemType.LABEL
        },
        {
            'type': UI_ItemType.SEPARATOR
        },
        {
            'text': SelectOperationRadioValues.CONVERT,
            'group': operation_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'text': SelectOperationRadioValues.COMPARE,
            'group': operation_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'type': UI_ItemType.SEPARATOR
        },
        {
            'text': 'Продолжить',
            'type': UI_ItemType.BUTTON
        },
    ]

    def generate_state():
        if operation_group.get() != '':
            operation_type = [SelectOperationRadioValues[operation_group.get()]]
        return operation_type

    def render_items(state: list, window):
        window.clear_window()
        
        for option in options:
            
            if (option['type'] == UI_ItemType.RADIOBUTTON):
                RadioButtonWidget(
                    window.window,
                    option['text'].value,
                    option['group'],
                    option['text'].name,
                    lambda: render_items(generate_state(), window)).pack()
            elif (option['type'] == UI_ItemType.SEPARATOR):
                ttk.Separator(window.window, orient='horizontal').pack(fill='x')
            elif (option['type'] == UI_ItemType.BUTTON):
                ButtonWidget(window.window, option['text'], lambda: command(state)).pack(padx=PAD_X, pady=PAD_Y)
            elif (option['type'] == UI_ItemType.LABEL):
                LabelWidget(window.window, option['text']).pack()

    render_items(generate_state(), window)

    state.clear()