import threading
from ui.widgets.button import ButtonWidget
from ui.widgets.label import LabelWidget
from ui.widgets.entry import EntryWidget
from ui.widgets.window import MainWindow
from table_utilits import PAD_X, PAD_Y
from ui.loader.loader import LoaderApp
from table_utilits import compare_tables
from table_utilits import mark_initial_values

class ExcelComparator:
    def __init__(self, user_direct, user_input_sheet, table_type, window: MainWindow):
        self._user_direct = user_direct
        self._user_input_sheet = user_input_sheet
        self._table_type = table_type
        self._window = window
        self._app = None
        self._button = None
        self._comparison_thread = None

    def _on_comparison_complete(self, mark_initial_values_result):
        self._app.conversion_result = mark_initial_values_result
        self._app.stop_loader()
        self._button.update_attribute("state", "active")

    def _perform_comparison(self):
        self._app.start_loader()
        comparison_result = compare_tables(self._user_direct, self._user_input_sheet, self._table_type)
        mark_initital_values_result = mark_initial_values(comparison_result, self._user_input_sheet, self._table_type)
        self._on_comparison_complete(mark_initital_values_result)

    def _show_save_converted_file_window(self):
        LabelWidget(self._window.window, "Пожалуйста, введите наименование для сохраняемого файла:").pack()
        entry = EntryWidget(self._window.window)
        entry.pack()

        self._button = ButtonWidget(
            self._window.window,
            "Сохранить",
            lambda: self._save_file_handler(entry.widget.get()),
        )
        self._button.pack(padx=PAD_X, pady=PAD_Y)
        self._button.update_attribute("state", "disabled")

    def _save_file_handler(self, save_text):
        self._app.conversion_result.save(save_text + ".xlsx")
        self._window.window.destroy()

    def start_comparison(self):
        self._app = LoaderApp(self._window, self._on_comparison_complete)
        self._comparison_thread = threading.Thread(target=self._perform_comparison)
        self._comparison_thread.start()

        self._show_save_converted_file_window()
        self._window.run()
