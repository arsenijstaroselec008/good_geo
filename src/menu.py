import customtkinter as ctk
from tkinter import ttk
from typing import Callable
from table_utilits import PAD_X, PAD_Y
from table_utilits import GroupsRadioValues
from table_utilits import Material
from table_utilits import UI_ItemType
from table_utilits import TempalteRadioValues
from ui.widgets.radiobutton import RadioButtonWidget
from ui.widgets.button import ButtonWidget
from ui.widgets.label import LabelWidget
from table_utilits import SelectOperationRadioValues

def generate_radio_buttons(window, command: Callable, select_operation_state: list[SelectOperationRadioValues]):
    template_group = ctk.StringVar(value=TempalteRadioValues.OPZ.name)
    groups_group = ctk.StringVar(value=GroupsRadioValues.SUMMARY_TABLE_GROUP.name)
    material_group = ctk.StringVar(value=Material.OIL_AND_DISSOLVED_GAS.name)
    state = []

    options = [
        {
            'condition':lambda state: True,
            'text': 'Выберите шаблон',
            'type': UI_ItemType.LABEL
        },
        {
            'condition':lambda state: True,
            'type': UI_ItemType.SEPARATOR
        },
        {
            'condition':lambda state: True,
            'text': TempalteRadioValues.OPZ,
            'group': template_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: True,
            'text': TempalteRadioValues.PZ,
            'group': template_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: True,
            'text': TempalteRadioValues.CKR,
            'group': template_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.OPZ in state or TempalteRadioValues.PZ in state,
            'type': UI_ItemType.SEPARATOR
        },
        {
            'condition':lambda state: TempalteRadioValues.OPZ in state or TempalteRadioValues.PZ in state,
            'text': GroupsRadioValues.SUMMARY_TABLE_GROUP,
            'group': groups_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.OPZ in state or TempalteRadioValues.PZ in state,
            'text': GroupsRadioValues.MATCHING_TABLE_GROUP,
            'group': groups_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.OPZ in state or TempalteRadioValues.PZ in state,
            'text': GroupsRadioValues.STATE_CHANGE_GROUP,
            'group': groups_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.CKR in state,
            'text': GroupsRadioValues.OIL_STOCK_STATE,
            'group': groups_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.CKR in state,
            'text': GroupsRadioValues.DISSOLVED_GAS_STATE,
            'group': groups_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.CKR in state,
            'text': GroupsRadioValues.G1_GXH,
            'group': groups_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: GroupsRadioValues.SUMMARY_TABLE_GROUP in state or TempalteRadioValues.CKR in state,
            'type': UI_ItemType.SEPARATOR
        },
        {
            'condition':lambda state: GroupsRadioValues.SUMMARY_TABLE_GROUP in state,
            'text': Material.OIL_AND_DISSOLVED_GAS,
            'group': material_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: GroupsRadioValues.SUMMARY_TABLE_GROUP in state,
            'text': Material.FREE_GAS_GGH_CONDENSATE,
            'group': material_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.PZ in state and GroupsRadioValues.SUMMARY_TABLE_GROUP in state,
            'text': Material.DISSOLVED_GAS_COMPONENTS,
            'group': material_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.PZ in state and GroupsRadioValues.SUMMARY_TABLE_GROUP in state,
            'text': Material.TABLE_1_4,
            'group': material_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition': lambda state: TempalteRadioValues.PZ in state and GroupsRadioValues.SUMMARY_TABLE_GROUP in state,
            'text':Material.TABLE_1_5,
            'group':material_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.PZ in state and GroupsRadioValues.SUMMARY_TABLE_GROUP in state,
            'text': Material.SUMMARY_TABLE_OF_OIL_AND_SULFUR,
            'group': material_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.CKR in state,
            'text': Material.DISSOLVED_GAS_COMPONENTS,
            'group': material_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.CKR in state,
            'text': Material.FREE_GAS_COMPONENTS,
            'group': material_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.CKR in state,
            'text': Material.GAS_CAP_COMPONENTS,
            'group': material_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.CKR in state,
            'text': Material.SULFUR,
            'group': material_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.CKR in state,
            'text': Material.SUMMARY_TABLE_OIL_ADDITIONAL_COLUMNS,
            'group': material_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.CKR in state,
            'text': Material.SUMMARY_TABLE_GAS_ADDITIONAL_COLUMNS,
            'group': material_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: TempalteRadioValues.OPZ in state,
            'text': Material.OIL_AND_DISSOLVED_GAS_EXTRA_COLS,
            'group': material_group,
            'type': UI_ItemType.RADIOBUTTON
        },
        {
            'condition':lambda state: any(isinstance(item, Material) for item in state),
            'text': 'Продолжить',
            'type': UI_ItemType.BUTTON
        },
    ]

    def generate_state():
        template_list, groups_list, material_list  = [], [], []
        if template_group.get() != '':
            template_list = [TempalteRadioValues[template_group.get()]]
        if groups_group.get() != '':
            groups_list = [GroupsRadioValues[groups_group.get()]]
        if material_group.get() != '':
            material_list  = [Material[material_group.get()]]
        return select_operation_state + template_list + groups_list + material_list

    def render_items(state: list, window):
        window.clear_window()

        for option in options:
            if option['condition'](state):
                if (option['type'] == UI_ItemType.RADIOBUTTON):
                    RadioButtonWidget(
                        window.window,
                        option['text'].value,
                        option['group'],
                        option['text'].name,
                        lambda: render_items(generate_state(), window)).pack()
                elif (option['type'] == UI_ItemType.SEPARATOR):
                    ttk.Separator(window.window, orient='horizontal').pack(fill='x')
                elif (option['type'] == UI_ItemType.BUTTON):
                    ButtonWidget(window.window, option['text'], lambda: command(state)).pack(padx=PAD_X, pady=PAD_Y)
                elif (option['type'] == UI_ItemType.LABEL):
                    LabelWidget(window.window, option['text']).pack()

    render_items(generate_state(), window)

    state.clear()
    